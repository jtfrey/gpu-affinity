//
// CPUMapping.h
//
// API to keep track of CPUs associated with GPUs and the
// CPUs that have been allocated from that set.
//

#ifndef __CPUMAPPING_H__
#define __CPUMAPPING_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "BitVector.h"

typedef struct CPUMapping * CPUMappingRef;

bool CPUMappingIsValidAssignmentPattern(const char *assignmentPattern, const char* *errorAtChar);

CPUMappingRef CPUMappingWithCPUAffinities(BitVectorRef cpuAffinities, const char *assignmentPattern);

BitVectorRef CPUMappingGetCPUAffinities(CPUMappingRef theMapping);
BitVectorRef CPUMappingGetCPUAssignments(CPUMappingRef theMapping);

bool CPUMappingAssignNextCPU(CPUMappingRef theMapping, unsigned int *outCPUId);


#ifdef __cplusplus
} // extern "C"
#endif

#endif /* __CPUMAPPING_H__ */
