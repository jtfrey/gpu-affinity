//
// Message.h
//
// Simple logging functions.
//

#ifndef __MESSAGE_H__
#define __MESSAGE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <stdbool.h>

/*
 * The following CPP acrobatics produce a C string with the format
 * "<source file>:<line number>"
 */
#define MESSAGE_STRINGIFY(X)  #X
#define MESSAGE_TO_STRING(X)  MESSAGE_STRINGIFY(X)
#define MESSAGE_MARK          __FILE__ ":" MESSAGE_TO_STRING(__LINE__)

/*!
 * @enum
 */
enum {
    MessageVerbosityQuiet = 0,
    MessageVerbosityError,
    MessageVerbosityWarning,
    MessageVerbosityInfo,
    MessageVerbosityDebug,
    MessageVerbosityMax,

    MessageVerbosityDefault = MessageVerbosityError
};

enum {
    MessageOptionsNoNewline         = 1 << 0,
    MessageOptionsNoLevelTag        = 1 << 1,
    MessageOptionsNoMark            = 1 << 2,
    MessageOptionsShouldExit        = 1 << 3
};

int MessageGetLevel(void);
int MessageSetLevel(int newMessageLevel);

int Message(int level, unsigned int options, int rc, FILE *stream, const char *mark, const char *format, ...);

#define ERROR(OPT, RC, F, ...) \
        Message(MessageVerbosityError, (OPT), (RC), stderr, MESSAGE_MARK, F, ##__VA_ARGS__)
#define WARN(OPT, F, ...) \
        Message(MessageVerbosityWarning, (OPT), 0, stderr, MESSAGE_MARK, F, ##__VA_ARGS__)
#define INFO(OPT, F, ...) \
        Message(MessageVerbosityInfo, (OPT), 0, stderr, MESSAGE_MARK, F, ##__VA_ARGS__)
#define DEBUG(OPT, F, ...) \
        Message(MessageVerbosityDebug, (OPT), 0, stderr, MESSAGE_MARK, F, ##__VA_ARGS__)

#define ERROR_NO_MARK(OPT, RC, F, ...) \
        Message(MessageVerbosityError, (OPT)|MessageOptionsNoMark, RC, stderr, MESSAGE_MARK, F, ##__VA_ARGS__)
#define WARN_NO_MARK(OPT, F, ...) \
        Message(MessageVerbosityWarning, (OPT)|MessageOptionsNoMark, 0, stderr, MESSAGE_MARK, F, ##__VA_ARGS__)
#define INFO_NO_MARK(OPT, F, ...) \
        Message(MessageVerbosityInfo, (OPT)|MessageOptionsNoMark, 0, stderr, MESSAGE_MARK, F, ##__VA_ARGS__)
#define DEBUG_NO_MARK(OPT, F, ...) \
        Message(MessageVerbosityDebug, (OPT)|MessageOptionsNoMark, 0, stderr, MESSAGE_MARK, F, ##__VA_ARGS__)

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* __MESSAGE_H__ */
