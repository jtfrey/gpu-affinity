//
// CPUMapping.c
//
// API to keep track of CPUs associated with GPUs and the
// CPUs that have been allocated from that set.
//

#include "CPUMapping.h"
#include "Message.h"

typedef struct CPUMapping {
    struct CPUMapping   *link;
    BitVectorRef        cpuAffinities;
    unsigned int        lowCpuId, highCpuId;
    BitVectorRef        cpuAssignments;
    const char          *assignmentPatternBase, *assignmentPatternCurrent;
} CPUMapping;

//

CPUMapping*
__CPUMappingAlloc(
    const char          *assignmentPattern
)
{
    size_t              newObjectSize = sizeof(CPUMapping) + strlen(assignmentPattern) + 1;
    CPUMapping          *newObject = (CPUMapping*)malloc(newObjectSize);

    if ( newObject ) {
        newObject->link = NULL;
        newObject->cpuAffinities = NULL;
        newObject->cpuAssignments = NULL;
        newObject->assignmentPatternBase = ((void*)newObject) + sizeof(CPUMapping);
        newObject->assignmentPatternCurrent = newObject->assignmentPatternBase;
        strcpy((char*)newObject->assignmentPatternBase, assignmentPattern);
    }
    return newObject;
}

//

bool
CPUMappingIsValidAssignmentPattern(
    const char      *assignmentPattern,
    const char*     *errorAtChar
)
{
    if ( assignmentPattern && *assignmentPattern ) {
        while ( *assignmentPattern ) {
            switch (*assignmentPattern) {

                case 'l':
                case 'L':
                case '-':
                case 'h':
                case 'H':
                case '+':
                    break;

                default:
                    if ( errorAtChar ) *errorAtChar = assignmentPattern;
                    return false;

            }
            assignmentPattern++;
        }
        return true;
    }
    if ( errorAtChar ) *errorAtChar = assignmentPattern;
    return false;
}

//

CPUMappingRef
CPUMappingWithCPUAffinities(
    BitVectorRef        cpuAffinities,
    const char          *assignmentPattern
)
{
    static CPUMapping   *allocatedMappings = NULL;
    CPUMapping          *mapping = NULL;
    unsigned int        lowCpuId, highCpuId;
    
    if ( ! BitVectorLowestBitWithValue(cpuAffinities, 1, &lowCpuId) ) return NULL;
    if ( ! BitVectorHighestBitWithValue(cpuAffinities, 1, &highCpuId) ) return NULL;
    if ( allocatedMappings ) {
        mapping = allocatedMappings;
        while ( mapping ) {
            if ( BitVectorIsEqual(mapping->cpuAffinities, cpuAffinities) ) {
                if ( (lowCpuId == mapping->lowCpuId) && (highCpuId == mapping->highCpuId) ) return (CPUMappingRef)mapping;
            }
            mapping = mapping->link;
        }
    }

    // Not an already allocated affinity list:
    if ( CPUMappingIsValidAssignmentPattern(assignmentPattern, NULL) ) {
        mapping = __CPUMappingAlloc(assignmentPattern);
        if ( mapping ) {
            mapping->cpuAffinities = BitVectorCopy(cpuAffinities);
            mapping->lowCpuId = lowCpuId;
            mapping->highCpuId = highCpuId;
            mapping->cpuAssignments = BitVectorCreate(BitVectorGetBitCapacity(cpuAffinities));
            
            DEBUG(0, "new mapping object allocated (range %u - %u)", mapping->lowCpuId, mapping->highCpuId);

            // New mapping becomes head of list:
            mapping->link = allocatedMappings;
            allocatedMappings = mapping;
        }
    }
    return mapping;
}

//

BitVectorRef
CPUMappingGetCPUAffinities(
    CPUMappingRef       theMapping
)
{
    return theMapping->cpuAffinities;
}

//

BitVectorRef
CPUMappingGetCPUAssignments(
    CPUMappingRef       theMapping
)
{
    return theMapping->cpuAssignments;
}

//

bool
CPUMappingAssignNextCPU(
    CPUMappingRef       theMapping,
    unsigned int        *outCPUId
)
{
    switch ( *(theMapping->assignmentPatternCurrent) ) {
        case 'l':
        case 'L':
        case '-':
            if ( ! BitVectorLowestBitWithValueInRange(theMapping->cpuAssignments, false, theMapping->lowCpuId, theMapping->highCpuId, outCPUId) ) return false;
            break;
        case 'h':
        case 'H':
        case '+':
            if ( ! BitVectorHighestBitWithValueInRange(theMapping->cpuAssignments, false, theMapping->lowCpuId, theMapping->highCpuId, outCPUId) ) return false;
            break;
    }
    // Okay, it's been assigned:
    BitVectorSetBit(theMapping->cpuAssignments, *outCPUId, true);

    // Step the assignment pattern:
    theMapping->assignmentPatternCurrent++;
    if ( ! *(theMapping->assignmentPatternCurrent) ) theMapping->assignmentPatternCurrent = theMapping->assignmentPatternBase;
    return true;
}

