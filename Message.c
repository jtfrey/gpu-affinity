//
// Message.c
//
// Simple logging functions.
//

#include "Message.h"
#include <stdlib.h>
#include <stdarg.h>

static int              __MessageLevel = MessageVerbosityDefault;

const char*             __MessageLevelStrings[] = {
                                "QUIET",
                                "ERROR",
                                "WARNING",
                                "INFO",
                                "DEBUG"
                            };

//

int
MessageGetLevel(void)
{
    return __MessageLevel;
}

//

int
MessageSetLevel(
    int     newMessageLevel
)
{
    if ( newMessageLevel < MessageVerbosityQuiet ) newMessageLevel = MessageVerbosityQuiet;
    else if ( newMessageLevel >= MessageVerbosityMax ) newMessageLevel = MessageVerbosityMax - 1;
    __MessageLevel = newMessageLevel;
    
    INFO(MessageOptionsNoMark, "enabled messaging level %s", __MessageLevelStrings[newMessageLevel]);
    return __MessageLevel;
}

//

int
Message(
    int                 level,
    unsigned int        options,
    int                 rc,
    FILE                *stream,
    const char          *mark,
    const char          *format,
    ...
)
{
    va_list             vargs;
    int                 our_rc = 0;

    va_start(vargs, format);
    if ( level <= __MessageLevel ) {
        if ( (options & MessageOptionsNoLevelTag) == 0 ) our_rc += fprintf(stream, "[%s] ", __MessageLevelStrings[level]);
        our_rc += vfprintf(stream, format, vargs);
        if ( (options & MessageOptionsNoMark) == 0 ) our_rc += fprintf(stream, " (%s)", mark);
        if ( (options & MessageOptionsNoNewline) == 0 ) our_rc += fprintf(stream, "\n");
    } else {
        our_rc = 0;
    }
    va_end(vargs);

    if ( (options & MessageOptionsShouldExit) == MessageOptionsShouldExit ) exit(rc);
    return our_rc;
}
