//
// BitVector.c
//
// A simple array of bits.
//

#include "BitVector.h"

#define BIT_IN_POSITION(N)  ((BitVectorSubType)1UL << (N))

#define BitVectorArrayLengthForCapacity(CAPACITY) (((CAPACITY) + BitVectorBitsPerSubType - 1) / (BitVectorBitsPerSubType))

#define BitVectorSubTypeFilled ULONG_MAX

typedef struct BitVector {
    unsigned int        bitCapacity;
    unsigned int        arrayLength;
    BitVectorSubType    *bits;
} BitVector;

//

BitVectorRef
BitVectorCreate(
    unsigned int        capacity
)
{
    BitVector           *newObject = malloc(sizeof(BitVector));

    if ( newObject ) {
        if ( capacity < 1 ) capacity = 1;

        newObject->bitCapacity = capacity;
        newObject->arrayLength = BitVectorArrayLengthForCapacity(capacity);
        newObject->bits = calloc(newObject->arrayLength, sizeof(BitVectorSubType));
    }
    return (BitVectorRef)newObject;
}

//

BitVectorRef
BitVectorCopy(
    BitVectorRef        bitVector
)
{
    BitVector           *newObject = BitVectorCreate(bitVector->bitCapacity);

    if ( newObject ) {
        BitVectorSubType    *b1 = bitVector->bits;
        BitVectorSubType    *b2 = newObject->bits;
        unsigned int        length = bitVector->arrayLength;

        while ( length-- ) *b2++ = *b1++;
    }
    return newObject;
}

//

void
BitVectorDestroy(
    BitVectorRef        bitVector
)
{
    if ( bitVector->bits ) free((void*)bitVector->bits);
    free((void*)bitVector);
}

//

BitVectorSubType*
BitVectorGetBitArrayPtr(
    BitVectorRef        bitVector
)
{
    return bitVector->bits;
}

//

unsigned int
BitVectorGetBitCapacity(
    BitVectorRef        bitVector
)
{
    return bitVector->bitCapacity;
}

//

unsigned int
BitVectorGetArrayLength(
    BitVectorRef        bitVector
)
{
    return bitVector->arrayLength;
}

//

void
BitVectorClear(
    BitVectorRef        bitVector
)
{
    memset(bitVector->bits, 0, sizeof(BitVectorSubType) * bitVector->arrayLength);
}

//

bool
BitVectorIsZero(
    BitVectorRef    bitVector
)
{
    unsigned int    i = 0;
    unsigned int    bitIndex = bitVector->bitCapacity;

    while ( (bitIndex > 0) && (i < bitVector->arrayLength) ) {
        if ( bitIndex >= BitVectorBitsPerSubType ) {
            if ( bitVector->bits[i] != 0 ) return false;
            bitIndex -= BitVectorBitsPerSubType;
        } else {
            BitVectorSubType    mask = 0;

            while ( bitIndex > 0 ) {
                mask = (mask << 1) | 1;
                bitIndex--;
            }
            if ( (bitVector->bits[i] & mask) != 0 ) return false;
        }
        i++;
    }
    return true;
}

//

bool
BitVectorIsEqual(
    BitVectorRef        bitVector1,
    BitVectorRef        bitVector2
)
{
    if ( bitVector1->bitCapacity == bitVector2->bitCapacity ) {
        unsigned int        bitCount = bitVector1->bitCapacity;
        BitVectorSubType    *v1 = bitVector1->bits;
        BitVectorSubType    *v2 = bitVector1->bits;

        while ( bitCount >= BitVectorBitsPerSubType ) {
            if ( *v1 != *v2 ) return false;
            bitCount -= BitVectorBitsPerSubType;
            v1++; v2++;
        }
        if ( bitCount > 0 ) {
            BitVectorSubType    mask = 0;

            while ( bitCount-- ) mask = (mask << 1) | (BitVectorSubType)1;
            if ( (*v1 & mask) != (*v2 & mask) ) return false;
        }
        return true;
    }
    return false;
}

//

bool
BitVectorGetBit(
    BitVectorRef        bitVector,
    unsigned int        bitIndex,
    bool                *bitValue
)
{
    if ( bitIndex >= bitVector->bitCapacity ) {
        fprintf(stderr, "ERROR:  invalid bit index (%u >= %u)\n", bitIndex, bitVector->bitCapacity);
        return false;
    }

    unsigned int        majIdx = bitIndex / BitVectorBitsPerSubType;
    unsigned int        minIdx = bitIndex % BitVectorBitsPerSubType;

    *bitValue = (bitVector->bits[majIdx] & BIT_IN_POSITION(minIdx)) ? true : false;
    return true;
}

//

bool
BitVectorGetBitOnly(
    BitVectorRef        bitVector,
    unsigned int        bitIndex
)
{
    if ( bitIndex >= bitVector->bitCapacity ) {
        fprintf(stderr, "ERROR:  invalid bit index (%u >= %u)\n", bitIndex, bitVector->bitCapacity);
        return false;
    }

    unsigned int        majIdx = bitIndex / BitVectorBitsPerSubType;
    unsigned int        minIdx = bitIndex % BitVectorBitsPerSubType;

    return (bitVector->bits[majIdx] & BIT_IN_POSITION(minIdx)) ? true : false;
}

//

bool
BitVectorEnumerate(
    BitVectorRef        bitVector,
    BitVectorEnumState  *enumState,
    bool                shouldContinue,
    BitVectorEnumerator enumeratorFunction,
    const void          *context
)
{
    bool                rc = true;

    if ( ! shouldContinue ) {
        if ( enumState->startBitIndex >= bitVector->bitCapacity ) {
            fprintf(stderr, "ERROR:  invalid start bit index (%u >= %u)\n", enumState->startBitIndex, bitVector->bitCapacity);
            exit(EINVAL);
        }
        if ( enumState->endBitIndex == BitIndexMax ) enumState->endBitIndex = bitVector->bitCapacity - 1;
        if ( (enumState->endBitIndex >= bitVector->bitCapacity) || (enumState->startBitIndex > enumState->endBitIndex) ) {
            fprintf(stderr, "ERROR:  invalid end bit index (%1$u >= %2$u || %3$u > %1$u)\n", enumState->endBitIndex, bitVector->bitCapacity, enumState->startBitIndex);
            exit(EINVAL);
        }

        enumState->startArrayMajorIndex = enumState->startBitIndex / BitVectorBitsPerSubType;
        enumState->startArrayMinorIndex = enumState->startBitIndex % BitVectorBitsPerSubType;
        enumState->endArrayMajorIndex = enumState->endBitIndex / BitVectorBitsPerSubType;
        enumState->endArrayMinorIndex = enumState->endBitIndex % BitVectorBitsPerSubType;
        enumState->curBitIndex = enumState->startBitIndex;
        enumState->curArrayMajorIndex = enumState->startArrayMajorIndex;
        enumState->curArrayMinorIndex = enumState->startArrayMinorIndex;
    }

    while ( rc && (enumState->curArrayMajorIndex <= enumState->endArrayMajorIndex) ) {
        BitVectorSubType    chunk = bitVector->bits[enumState->curArrayMajorIndex];
        unsigned int        startIdx = (enumState->curArrayMajorIndex == enumState->startArrayMajorIndex) ? enumState->startArrayMinorIndex : 0;
        unsigned int        endIdx = (enumState->curArrayMajorIndex == enumState->endArrayMajorIndex) ? enumState->endArrayMinorIndex : (BitVectorBitsPerSubType - 1);

        while ( rc && (startIdx <= endIdx) ) {
            rc = enumeratorFunction(bitVector, enumState->curBitIndex++, (chunk & BIT_IN_POSITION(startIdx++)) ? true : false, context);
        }
        enumState->curArrayMajorIndex++;
    }
    return rc;
}

bool
BitVectorEnumerateQuick(
    BitVectorRef        bitVector,
    unsigned int        startBitIndex,
    unsigned int        endBitIndex,
    BitVectorEnumerator enumeratorFunction,
    const void          *context
)
{
    BitVectorEnumState  eState = { .startBitIndex = startBitIndex, .endBitIndex = endBitIndex };

    return BitVectorEnumerate(bitVector, &eState, false, enumeratorFunction, context);
}

//

bool
BitVectorReverseEnumerate(
    BitVectorRef        bitVector,
    BitVectorEnumState  *enumState,
    bool                shouldContinue,
    BitVectorEnumerator enumeratorFunction,
    const void          *context
)
{
    bool                rc = true;

    if ( ! shouldContinue ) {
        if ( enumState->startBitIndex >= bitVector->bitCapacity ) {
            fprintf(stderr, "ERROR:  invalid start bit index (%u >= %u)\n", enumState->startBitIndex, bitVector->bitCapacity);
            exit(EINVAL);
        }
        if ( enumState->endBitIndex == BitIndexMax ) enumState->endBitIndex = bitVector->bitCapacity - 1;
        if ( (enumState->endBitIndex >= bitVector->bitCapacity) || (enumState->startBitIndex > enumState->endBitIndex) ) {
            fprintf(stderr, "ERROR:  invalid end bit index (%1$u >= %2$u || %3$u > %1$u)\n", enumState->endBitIndex, bitVector->bitCapacity, enumState->startBitIndex);
            exit(EINVAL);
        }

        enumState->startArrayMajorIndex = enumState->startBitIndex / BitVectorBitsPerSubType;
        enumState->startArrayMinorIndex = enumState->startBitIndex % BitVectorBitsPerSubType;
        enumState->endArrayMajorIndex = enumState->endBitIndex / BitVectorBitsPerSubType;
        enumState->endArrayMinorIndex = enumState->endBitIndex % BitVectorBitsPerSubType;
        enumState->curBitIndex = enumState->endBitIndex;
        enumState->curArrayMajorIndex = enumState->endArrayMajorIndex;
        enumState->curArrayMinorIndex = enumState->endArrayMinorIndex;
    }

    while ( rc && (enumState->curArrayMajorIndex >= enumState->startArrayMajorIndex) ) {
        BitVectorSubType    chunk = bitVector->bits[enumState->curArrayMajorIndex];
        unsigned int        startIdx = (enumState->curArrayMajorIndex == enumState->startArrayMajorIndex) ? enumState->startArrayMinorIndex : 0;
        unsigned int        endIdx = (enumState->curArrayMajorIndex == enumState->endArrayMajorIndex) ? enumState->endArrayMinorIndex : (BitVectorBitsPerSubType - 1);

        while ( rc && (startIdx <= endIdx) ) {
            rc = enumeratorFunction(bitVector, enumState->curBitIndex--, (chunk & BIT_IN_POSITION(endIdx)) ? true : false, context);
            if ( endIdx == 0 ) break;
            endIdx--;
        }
        if ( enumState->curArrayMajorIndex == 0 ) break;
        enumState->curArrayMajorIndex--;
    }
    return rc;
}

bool
BitVectorReverseEnumerateQuick(
    BitVectorRef        bitVector,
    unsigned int        startBitIndex,
    unsigned int        endBitIndex,
    BitVectorEnumerator enumeratorFunction,
    const void          *context
)
{
    BitVectorEnumState  eState = { .startBitIndex = startBitIndex, .endBitIndex = endBitIndex };

    return BitVectorReverseEnumerate(bitVector, &eState, false, enumeratorFunction, context);
}

//

#define BITVECTOR_MAJOR_INDEX_OF_BIT(BNUM)      (BNUM / BitVectorBitsPerSubType)
#define BITVECTOR_MINOR_INDEX_OF_BIT(BNUM)      (BNUM % BitVectorBitsPerSubType)

//

bool
BitVectorHighestBitWithValueInRange(
    BitVectorRef        bitVector,
    bool                bitValue,
    unsigned int        startBitIndex,
    unsigned int        endBitIndex,
    unsigned int        *outBitIndex
)
{
    if ( startBitIndex >= bitVector->bitCapacity ) return false;
    if ( endBitIndex >= bitVector->bitCapacity ) return false;

    unsigned int        majIdx, minIdx;

    if ( bitValue ) {
        while ( endBitIndex >= startBitIndex ) {
            BitVectorSubType    chunk = bitVector->bits[BITVECTOR_MAJOR_INDEX_OF_BIT(endBitIndex)];

            if ( chunk != 0 ) {
                if ( (chunk & (BIT_IN_POSITION(BITVECTOR_MINOR_INDEX_OF_BIT(endBitIndex)))) != 0 ) {
                    *outBitIndex = endBitIndex;
                    return true;
                }
            }
            if ( endBitIndex == 0 ) break;
            endBitIndex--;
        }
    } else {
        while ( endBitIndex >= startBitIndex ) {
            BitVectorSubType    chunk = bitVector->bits[BITVECTOR_MAJOR_INDEX_OF_BIT(endBitIndex)];

            if ( chunk != BitVectorSubTypeFilled ) {
                if ( (chunk & (BIT_IN_POSITION(BITVECTOR_MINOR_INDEX_OF_BIT(endBitIndex)))) == 0 ) {
                    *outBitIndex = endBitIndex;
                    return true;
                }
            }
            if ( endBitIndex == 0 ) break;
            endBitIndex--;
        }
    }
    return false;
}

//

bool
BitVectorHighestBitWithValue(
    BitVectorRef        bitVector,
    bool                bitValue,
    unsigned int        *outBitIndex
)
{
    unsigned int        endIndex = bitVector->bitCapacity - 1;
    unsigned int        chunkIndex = bitVector->arrayLength;
    unsigned int        testBitIndex = endIndex % BitVectorBitsPerSubType;

    while ( chunkIndex-- ) {
        BitVectorSubType    chunk = bitVector->bits[chunkIndex];

        if ( bitValue && chunk ) {
            BitVectorSubType    mask = BIT_IN_POSITION(testBitIndex);

            // Which bit is set?
            while ( mask ) {
                if ( chunk & mask ) {
                    *outBitIndex = endIndex;
                    return true;
                }
                mask >>= 1;
                endIndex--;
            }
        } else if ( ! bitValue && (chunk != BitVectorSubTypeFilled) ) {
            BitVectorSubType    mask = BIT_IN_POSITION(testBitIndex);

            // Which bit is not set?
            while ( mask ) {
                if ( (chunk & mask) == 0 ) {
                    *outBitIndex = endIndex;
                    return true;
                }
                mask >>= 1;
                endIndex--;
            }
        } else {
            endIndex -= BitVectorBitsPerSubType;
        }
        testBitIndex = BitVectorBitsPerSubType - 1;
    }
    return false;
}

//

bool
BitVectorLowestBitWithValueInRange(
    BitVectorRef        bitVector,
    bool                bitValue,
    unsigned int        startBitIndex,
    unsigned int        endBitIndex,
    unsigned int        *outBitIndex
)
{
    if ( startBitIndex >= bitVector->bitCapacity ) return false;
    if ( endBitIndex >= bitVector->bitCapacity ) return false;

    unsigned int        majIdx, minIdx;

    if ( bitValue ) {
        while ( startBitIndex <= endBitIndex ) {
            BitVectorSubType    chunk = bitVector->bits[BITVECTOR_MAJOR_INDEX_OF_BIT(startBitIndex)];

            if ( chunk != 0 ) {
                if ( (chunk & (BIT_IN_POSITION(BITVECTOR_MINOR_INDEX_OF_BIT(startBitIndex)))) != 0 ) {
                    *outBitIndex = startBitIndex;
                    return true;
                }
            }
            if ( startBitIndex == endBitIndex ) break;
            startBitIndex++;
        }
    } else {
        while ( startBitIndex <= endBitIndex ) {
            BitVectorSubType    chunk = bitVector->bits[BITVECTOR_MAJOR_INDEX_OF_BIT(startBitIndex)];

            if ( chunk != BitVectorSubTypeFilled ) {
                if ( (chunk & (BIT_IN_POSITION(BITVECTOR_MINOR_INDEX_OF_BIT(startBitIndex)))) == 0 ) {
                    *outBitIndex = startBitIndex;
                    return true;
                }
            }
            if ( startBitIndex == endBitIndex ) break;
            startBitIndex++;
        }
    }
    return false;
}

//

bool
BitVectorLowestBitWithValue(
    BitVectorRef        bitVector,
    bool                bitValue,
    unsigned int        *outBitIndex
)
{
    unsigned int        startIndex = 0;
    unsigned int        chunkIndex = 0;

    while ( chunkIndex < bitVector->arrayLength ) {
        BitVectorSubType    chunk = bitVector->bits[chunkIndex++];

        if ( bitValue && chunk ) {
            BitVectorSubType    mask = 1UL;

            // Which bit is set?
            while ( mask ) {
                if ( chunk & mask ) {
                    *outBitIndex = startIndex;
                    return true;
                }
                mask <<= 1;
                startIndex++;
            }
        } else if ( ! bitValue && (chunk != BitVectorSubTypeFilled) ) {
            BitVectorSubType    mask = 1UL;

            // Which bit is not set?
            while ( mask ) {
                if ( (chunk & mask) == 0 ) {
                    *outBitIndex = startIndex;
                    return true;
                }
                mask <<= 1;
                startIndex++;
            }
        } else {
            startIndex += BitVectorBitsPerSubType;
        }
    }
    return false;
}

//

bool
BitVectorSetBit(
    BitVectorRef        bitVector,
    unsigned int        bitIndex,
    bool                bitValue
)
{
    if ( bitIndex >= bitVector->bitCapacity ) {
        // Attempt to resize to accommodate this index:
        unsigned int        newArrayLength = BitVectorArrayLengthForCapacity(bitIndex + 1);

        if ( newArrayLength > bitVector->arrayLength ) {
            BitVectorSubType    *newArray = realloc(bitVector->bits, newArrayLength * sizeof(BitVectorSubType));

            if ( ! newArray ) {
                fprintf(stderr, "ERROR:  insufficient memory to extend bit vector\n");
                return false;
            }
            memset((newArray + bitVector->arrayLength), 0, sizeof(BitVectorSubType) * (newArrayLength - bitVector->arrayLength));
            bitVector->bits = newArray;
            bitVector->arrayLength = newArrayLength;
        } else {
            // Zero-out newly-activated bit ranges:
            if ( bitVector->arrayLength < newArrayLength ) memset((bitVector->bits + bitVector->arrayLength), 0, sizeof(BitVectorSubType) * (newArrayLength - bitVector->arrayLength));
        }
        bitVector->bitCapacity = bitIndex + 1;
    }

    unsigned int        majIdx = bitIndex / BitVectorBitsPerSubType;
    unsigned int        minIdx = bitIndex % BitVectorBitsPerSubType;

    if ( bitValue ) {
        bitVector->bits[majIdx] |= BIT_IN_POSITION(minIdx);
    } else {
        bitVector->bits[majIdx] &= ~BIT_IN_POSITION(minIdx);
    }
    return true;
}

//

bool
BitVectorSetBits(
    BitVectorRef        bitVector,
    unsigned int        startBitIndex,
    unsigned int        endBitIndex,
    bool                bitValue
)
{
    if ( startBitIndex > endBitIndex ) {
        fprintf(stderr, "ERROR:  invalid end bit indices (%u > %u)\n", startBitIndex, endBitIndex);
        return false;
    }
    if ( endBitIndex >= bitVector->bitCapacity ) {
        // Attempt to resize to accommodate this index:
        unsigned int        newArrayLength = BitVectorArrayLengthForCapacity(endBitIndex + 1);

        if ( newArrayLength > bitVector->arrayLength ) {
            BitVectorSubType    *newArray = realloc(bitVector->bits, newArrayLength * sizeof(BitVectorSubType));

            if ( ! newArray ) {
                fprintf(stderr, "ERROR:  insufficient memory to extend bit vector\n");
                return false;
            }
            memset((newArray + bitVector->arrayLength), 0, sizeof(BitVectorSubType) * (newArrayLength - bitVector->arrayLength));
            bitVector->bits = newArray;
            bitVector->arrayLength = newArrayLength;
        } else {
            // Zero-out newly-activated bit ranges:
            if ( bitVector->arrayLength < newArrayLength ) memset((bitVector->bits + bitVector->arrayLength), 0, sizeof(BitVectorSubType) * (newArrayLength - bitVector->arrayLength));
        }
        bitVector->bitCapacity = endBitIndex + 1;
    }

    unsigned int        startMajorIdx = startBitIndex / BitVectorBitsPerSubType;
    unsigned int        startMinorIdx = startBitIndex % BitVectorBitsPerSubType;
    unsigned int        endMajorIdx = endBitIndex / BitVectorBitsPerSubType;
    unsigned int        endMinorIdx = endBitIndex % BitVectorBitsPerSubType;
    unsigned int        curMajorIdx = startMajorIdx;

    while ( curMajorIdx <= endMajorIdx ) {
        BitVectorSubType    chunk = bitVector->bits[curMajorIdx];
        unsigned int        startIdx = (curMajorIdx == startMajorIdx) ? startMinorIdx : 0;
        unsigned int        endIdx = (curMajorIdx == endMajorIdx) ? endMinorIdx : (BitVectorBitsPerSubType - 1);

        // Whole word?
        if ( (startIdx == 0) && (endIdx == (BitVectorBitsPerSubType - 1)) ) {
            bitVector->bits[curMajorIdx] = bitValue ? BitVectorSubTypeFilled : 0UL;
        } else {
            BitVectorSubType    mask = 0;

            while ( startIdx <= endIdx ) {
                mask |= BIT_IN_POSITION(startIdx);
                startIdx++;
            }
            if ( bitValue ) {
                bitVector->bits[curMajorIdx] |= mask;
            } else {
                bitVector->bits[curMajorIdx] &= ~mask;
            }
        }
        curMajorIdx++;
    }
    return true;
}

//

BitVectorRef
BitVectorAnd(
    BitVectorRef    bitVector1,
    BitVectorRef    bitVector2,
    BitVectorRef    outBitVector
)
{
    // Figure out minimum capacity of input vectors:
    unsigned int    maxBitCapacity = bitVector1->bitCapacity;
    if ( bitVector2->bitCapacity > maxBitCapacity ) maxBitCapacity = bitVector2->bitCapacity;

    // If outBitVector is NULL, allocate a new vector:
    if ( ! outBitVector ) {
        outBitVector = BitVectorCreate(maxBitCapacity);
        if ( ! outBitVector ) return NULL;
    } else {
        // If the incoming bit vector isn't large enough, exit now:
        if ( (BitVectorBitsPerSubType * outBitVector->arrayLength) < maxBitCapacity ) return NULL;
        outBitVector->bitCapacity = maxBitCapacity;
    }

    unsigned int    i = 0, iMax = BitVectorArrayLengthForCapacity(maxBitCapacity);

    while ( i < iMax ) {
        outBitVector->bits[i] = bitVector1->bits[i] & bitVector2->bits[i];
        i++;
    }
    return outBitVector;
}

//

BitVectorRef
BitVectorOr(
    BitVectorRef    bitVector1,
    BitVectorRef    bitVector2,
    BitVectorRef    outBitVector
)
{
    // Figure out minimum capacity of input vectors:
    unsigned int    maxBitCapacity = bitVector1->bitCapacity;
    if ( bitVector2->bitCapacity > maxBitCapacity ) maxBitCapacity = bitVector2->bitCapacity;

    // If outBitVector is NULL, allocate a new vector:
    if ( ! outBitVector ) {
        outBitVector = BitVectorCreate(maxBitCapacity);
        if ( ! outBitVector ) return NULL;
    } else {
        // If the incoming bit vector isn't large enough, exit now:
        if ( (BitVectorBitsPerSubType * outBitVector->arrayLength) < maxBitCapacity ) return NULL;
        outBitVector->bitCapacity = maxBitCapacity;
    }

    unsigned int    i = 0, iMax = BitVectorArrayLengthForCapacity(maxBitCapacity);

    while ( i < iMax ) {
        outBitVector->bits[i] = bitVector1->bits[i] | bitVector2->bits[i];
        i++;
    }
    return outBitVector;
}

//

BitVectorRef
BitVectorXor(
    BitVectorRef    bitVector1,
    BitVectorRef    bitVector2,
    BitVectorRef    outBitVector
)
{
    // Figure out minimum capacity of input vectors:
    unsigned int    maxBitCapacity = bitVector1->bitCapacity;
    if ( bitVector2->bitCapacity > maxBitCapacity ) maxBitCapacity = bitVector2->bitCapacity;

    // If outBitVector is NULL, allocate a new vector:
    if ( ! outBitVector ) {
        outBitVector = BitVectorCreate(maxBitCapacity);
        if ( ! outBitVector ) return NULL;
    } else {
        // If the incoming bit vector isn't large enough, exit now:
        if ( (BitVectorBitsPerSubType * outBitVector->arrayLength) < maxBitCapacity ) return NULL;
        outBitVector->bitCapacity = maxBitCapacity;
    }

    unsigned int    i = 0, iMax = BitVectorArrayLengthForCapacity(maxBitCapacity);

    while ( i < iMax ) {
        outBitVector->bits[i] = bitVector1->bits[i] ^ bitVector2->bits[i];
        i++;
    }
    return outBitVector;
}

//

BitVectorRef
BitVectorNot(
    BitVectorRef    bitVector1,
    BitVectorRef    outBitVector
)
{
    // Figure out minimum capacity of input vectors:
    unsigned int    maxBitCapacity = bitVector1->bitCapacity;

    // If outBitVector is NULL, allocate a new vector:
    if ( ! outBitVector ) {
        outBitVector = BitVectorCreate(bitVector1->bitCapacity);
        if ( ! outBitVector ) return NULL;
    } else {
        // If the incoming bit vector isn't large enough, exit now:
        if ( (BitVectorBitsPerSubType * outBitVector->arrayLength) < bitVector1->bitCapacity ) return NULL;
        outBitVector->bitCapacity = bitVector1->bitCapacity;
    }

    unsigned int    i = 0, iMax = BitVectorArrayLengthForCapacity(bitVector1->bitCapacity);

    while ( i < iMax ) {
        outBitVector->bits[i] = ~bitVector1->bits[i];
        i++;
    }
    return outBitVector;
}

