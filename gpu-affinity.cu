//
// gpu-affinity
//
// Suggest CPU-to-GPU bindings for the sake of Gaussian '16 (and
// maybe other products).
//

#include "Message.h"
#include "BitVector.h"
#include "CPUMapping.h"
#include <getopt.h>
#include <errno.h>
#include <ctype.h>

#include <numa.h>

#include <nvml.h>
#include <cuda_runtime_api.h>

//

#ifndef GPU_AFFINITY_MAX_CPUS
#define GPU_AFFINITY_MAX_CPUS   256
#endif
unsigned int            gpuAffinityDefaultMaxCpus = GPU_AFFINITY_MAX_CPUS;

#ifndef GPU_AFFINITY_DEFAULT_ASSIGNMENT_PATTERN
#define GPU_AFFINITY_DEFAULT_ASSIGNMENT_PATTERN     "hl"
#endif
const char              *gpuAffinityDefaultAssignmentPattern = GPU_AFFINITY_DEFAULT_ASSIGNMENT_PATTERN;

//

const struct option     cliOptions[] = {
        { "verbose",        no_argument,        NULL,       'v' },
        { "quiet",          no_argument,        NULL,       'q' },
        { "help",           no_argument,        NULL,       'h' },
        { "show-cpu-list",  no_argument,        NULL,       's' },
        { "max-cpu",        required_argument,  NULL,       'c' },
        { "mapping-pattern",required_argument,  NULL,       'p' },
        { NULL,             0,                  NULL,       0   }
    };
const char              *cliOptionsString = "vqhsc:p:";

/*!
 * @function usage
 *     Display a usage summary and possibly exit.
 * @param exe The program name, argv[0]
 * @param isShortForm Display a shortened form of the usage summary
 * @param shouldExit After printing the information, exit with the
 *     given result code (rc)
 * @param rc Integer to return as program exits.
 */
void
usage(
    const char  *exe,
    bool        isShortForm,
    bool        shouldExit,
    int         rc
)
{
    if ( isShortForm ) {
        printf(
                "usage: %s [--help] [--verbose] [--quiet] [--show-cpu-list] [--max-cpu=#]\n"
                "          [--mapping-pattern=<pattern>]\n",
                exe
            );
    } else {
        printf(
                "usage:\n\n"
                "  %s {options}\n\n"
                " options:\n\n"
                "  --help/-h                     show this information\n"
                "  --verbose/-v                  display additional information on stderr as program\n"
                "                                executes (can be used multiple times)\n"
                "  --quiet/-q                    only print the desired output from this utility\n"
                "  --show-cpu-list/-s            also show the list of all CPUs that are adjacent to\n"
                "                                the available GPUs\n"
                "  --max-cpu=#, -c=#             use this integer as the maximum number of CPUs; defaults\n"
                "                                to %u, using the value 0 will request maximum possible\n"
                "                                CPUs from the NUMA tools\n"
                "  --mapping-pattern=<pattern>   when assigning CPUs to GPUs, choose CPU ids using the\n"
                "       -p=<pattern>             given <pattern>; if there are more GPUs than letters in\n"
                "                                the <pattern> assignment will cycle back to the start\n"
                "                                of <pattern>\n"
                "\n"
                " Syntax of <pattern>\n\n"
                "     A sequence of characters that specify how to select the primary CPU id that will be\n"
                "     bound to each visible GPU device.  The following characters are understood:\n\n"
                "         l, L, -                choose the lowest unused CPU id having affinity with\n"
                "                                the device\n"
                "         h, H, +                choose the highest unused CPU id having affinity with\n"
                "                                the device\n\n"
                "     The default <pattern> is '%s'.\n"
                "\n",
                exe,
                gpuAffinityDefaultMaxCpus,
                gpuAffinityDefaultAssignmentPattern
            );
    }
    if ( shouldExit ) exit(rc);
}

//

typedef struct GpuAssignment {
    struct GpuAssignment    *link;
    unsigned int            gpuId, cpuId;
} GpuAssignment;

//

void
GpuAssignmentAppend(
    GpuAssignment       **assignments,
    unsigned int        gpuId,
    unsigned int        cpuId
)
{
    GpuAssignment       *newAssignment = (GpuAssignment*)malloc(sizeof(GpuAssignment));

    if ( ! newAssignment ) {
        ERROR(MessageOptionsShouldExit, ENOMEM, "ERROR:  unable to allocate GPU assignment record");
    }
    newAssignment->link = NULL;
    newAssignment->gpuId = gpuId;
    newAssignment->cpuId = cpuId;

    GpuAssignment       *header = *assignments;

    if ( header ) {
        while ( header->link ) header = header->link;
        header->link = newAssignment;
    } else {
        *assignments = newAssignment;
    }
}

//

typedef struct IndexRange {
    unsigned int    start, end;
    bool            isFirst, isRangeStarted;
} IndexRange;

IndexRange*
IndexRangeInit(
    IndexRange      *indexRange
)
{
    indexRange->isFirst = true;
    indexRange->isRangeStarted = false;
    return indexRange;
}

void
IndexRangePrint(
    IndexRange      *indexRange
)
{
    if ( indexRange->isRangeStarted ) {
        if ( indexRange->start != indexRange->end ) {
            INFO(MessageOptionsNoMark|MessageOptionsNoNewline|MessageOptionsNoLevelTag, "%s%u-%u", indexRange->isFirst ? "" : ",", indexRange->start, indexRange->end);
        } else {
            INFO(MessageOptionsNoMark|MessageOptionsNoNewline|MessageOptionsNoLevelTag, "%s%u", indexRange->isFirst ? "" : ",", indexRange->start);
        }
        indexRange->isFirst = false;
    }
}

/*!
 * @function printSetBitIndices
 *     BitVector (forward) enumerator callback function that prints set bits
 *     as ranges.  As ranges are closed, the IndexRangePrint() function is
 *     called; to ensure the final bit range is printed the caller should
 *     follow the enumeration immediately by a call to IndexRangePrint()
 *     on the same IndexRange context.
 */
bool
printSetBitIndices(
    BitVectorRef    bitVector,
    unsigned int    bitIndex,
    bool            bitValue,
    const void      *context
)
{
    IndexRange      *CONTEXT = (IndexRange*)context;

    if ( bitValue ) {
        if ( CONTEXT->isRangeStarted ) {
            if ( bitIndex != (CONTEXT->end + 1) ) {
                // The bitIndex is not a part of the range being compiled,
                // so display that range and start a new one at bitIndex:
                IndexRangePrint(CONTEXT);
                CONTEXT->isRangeStarted = true;
                CONTEXT->isFirst = false;
                CONTEXT->start = CONTEXT->end = bitIndex;
            } else {
                // Extend the existing range being compiled:
                CONTEXT->end = bitIndex;
            }
        } else {
            // Starting a new range of set bits:
            CONTEXT->isRangeStarted = true;
            CONTEXT->start = CONTEXT->end = bitIndex;
        }
    } else if ( CONTEXT->isRangeStarted ) {
        // The current bit is NOT set, but a range was being compiled; print
        // that range and indicate there is no range being compiled now:
        IndexRangePrint(CONTEXT);
        CONTEXT->isRangeStarted = false;
    }
    // Continue on to next bit:
    return true;
}

/*!
 * @function cudaGetDeviceIndexByPCIParams
 *     Given a PCI device identification string, enumerate the visible
 *     CUDA devices and find the one with matching ids.  The CUDA API's
 *     index is the index that should be used by client software -- in
 *     this case, the L.H.S. index for Gaussian's GPUCPU mapping.
 * @param pciBusId The PCI bus id of the device
 * @param pciDeviceId The device id on the PCI bus
 * @param deviceIndex If the device is successfully located, this is
 *     filled with the CUDA index of the device.
 * @result Returns false in case of any error, true if successful.
 */
bool
cudaGetDeviceIndexByPCIParams(
    const char      *pciId,
    unsigned int    *deviceIndex
)
{
    int             deviceId = -1;
    cudaError_t     err = cudaDeviceGetByPCIBusId(&deviceId, pciId);

    if ( err == cudaSuccess ) {
        *deviceIndex = deviceId;
        return true;
    }
    ERROR(0, 0, "failed to find GPU with PCI id '%s' (err = %d)", pciId, err);
    return false;
}

//

int
main(
    int                     argc,
    char * const            argv[]
)
{
    int                     rc = 1;
    nvmlReturn_t            nvml_rc = NVML_SUCCESS;
    unsigned int            nDevices = 0;
    BitVectorRef            availCpus = NULL;
    BitVectorRef            deviceCpuMask = NULL;
    GpuAssignment           *gpuAssignments = NULL;
    int                     numPossibleCpus = gpuAffinityDefaultMaxCpus;
    const char              *assignmentPattern = gpuAffinityDefaultAssignmentPattern;
    bool                    shouldShowCpuList = false;
    int                     optCh;

    while ( (optCh = getopt_long(argc, argv, cliOptionsString, cliOptions, NULL)) != -1 ) {
        switch ( optCh ) {

            case 'h':
                usage(argv[0], false, true, 0);
                break;

            case 'v':
                MessageSetLevel(MessageGetLevel() + 1);
                break;
            case 'q':
                MessageSetLevel(MessageVerbosityQuiet);
                break;

            case 's':
                shouldShowCpuList = true;
                if ( MessageGetLevel() < MessageVerbosityInfo ) MessageSetLevel(MessageVerbosityInfo);
                break;

            case 'n': {
                if ( optarg ) while ( isspace(*optarg) ) optarg++;
                if ( optarg && *optarg ) {
                    char        *e;
                    long        v = strtol(optarg, &e, 0);

                    if ( e > optarg ) {
                        if ( v && (v < GPU_AFFINITY_MAX_CPUS) ) {
                            v = GPU_AFFINITY_MAX_CPUS;
                            INFO(0, "defaulting to maximum of %u CPUs", (unsigned int)GPU_AFFINITY_MAX_CPUS);
                        }
                        numPossibleCpus = v;
                    } else {
                        ERROR(0, 0, "invalid processor count limit: %s", optarg);
                        usage(argv[0], true, true, EINVAL);
                    }
                } else {
                    ERROR(0, 0, "no processor count limit provided with --max-cpu/-c");
                    usage(argv[0], true, true, EINVAL);
                }
                break;
            }

            case 'p': {
                if ( optarg ) while ( isspace(*optarg) ) optarg++;
                if ( optarg && *optarg ) {
                    const char      *errorAtChar = NULL;

                    if ( ! CPUMappingIsValidAssignmentPattern(optarg, &errorAtChar) ) {
                        const char  *format1 = "invalid assignment pattern: %s";
                        size_t      lenFormat1 = strlen(format1);
                        char        errorMark[lenFormat1 + (errorAtChar - optarg)];

                        snprintf(errorMark, sizeof(errorMark), "%*s^", (lenFormat1 - 2 + (errorAtChar - optarg)), "");
                        ERROR(0, 0, format1, optarg);
                        ERROR(MessageOptionsShouldExit | MessageOptionsNoMark, EINVAL, errorMark);
                    }
                    assignmentPattern = optarg;
                } else {
                    ERROR(0, 0, "no mapping pattern provided with --mapping-pattern/-p");
                    usage(argv[0], true, true, EINVAL);
                }
                break;
            }

        }
    }

    if ( numPossibleCpus <= 0 ) {
        numPossibleCpus = numa_num_possible_cpus();
        INFO(0, "system reports %d CPUs are possible", numPossibleCpus);
    }
    availCpus = BitVectorCreate(numPossibleCpus);
    deviceCpuMask = BitVectorCreate(numPossibleCpus);

    if ( (nvml_rc = nvmlInit()) != NVML_SUCCESS ) {
        ERROR(MessageOptionsShouldExit, 1, "%s (%d)", nvmlErrorString(nvml_rc), nvml_rc);
    }
    if ( (nvml_rc = nvmlDeviceGetCount_v2(&nDevices)) != NVML_SUCCESS ) {
        ERROR(0, 0, "%s (%d)", nvmlErrorString(nvml_rc), nvml_rc);
    }
    if ( nvml_rc == NVML_SUCCESS ) {
        unsigned int        iDevice = 0;
        nvmlDevice_t        aDevice;

        while ( iDevice < nDevices ) {
            if ( (nvml_rc = nvmlDeviceGetHandleByIndex_v2(iDevice, &aDevice)) != NVML_SUCCESS ) {
                ERROR(0, 0, "%s (%d)", nvmlErrorString(nvml_rc), nvml_rc);
            }
            if ( nvml_rc == NVML_SUCCESS ) {
                if ( (nvml_rc = nvmlDeviceGetCpuAffinity(aDevice, BitVectorGetArrayLength(deviceCpuMask), BitVectorGetBitArrayPtr(deviceCpuMask))) != NVML_SUCCESS ) {
                    ERROR(0, 0, "%s (%d)", nvmlErrorString(nvml_rc), nvml_rc);
                }
                if ( nvml_rc == NVML_SUCCESS ) {
                    nvmlPciInfo_t   devicePciId;

                    if ( (nvml_rc = nvmlDeviceGetPciInfo(aDevice, &devicePciId)) != NVML_SUCCESS ) {
                        ERROR(0, 0, "%s (%d)", nvmlErrorString(nvml_rc), nvml_rc);
                    }
                    if ( nvml_rc == NVML_SUCCESS ) {
                        unsigned int    deviceId;

                        if ( cudaGetDeviceIndexByPCIParams(devicePciId.busId, &deviceId) ) {
                            CPUMappingRef   theCPUMapping = NULL;

                            // Merge the CPU bits into the overall list of available CPUs:
                            BitVectorOr(availCpus, deviceCpuMask, availCpus);

                            // Get the CPUMapping object for this affinity mask:
                            theCPUMapping = CPUMappingWithCPUAffinities(deviceCpuMask, assignmentPattern);
                            if ( theCPUMapping ) {
                                unsigned int    cpuId;

                                if ( CPUMappingAssignNextCPU(theCPUMapping, &cpuId) ) {
                                    GpuAssignmentAppend(&gpuAssignments, deviceId, cpuId);
                                } else {
                                    ERROR(MessageOptionsShouldExit, EINVAL, "no free CPUs to associate with GPU %u", iDevice);
                                }
                            }
                        }
                    }
                }
            }
            iDevice++;
        }
    }

    if ( ! BitVectorIsZero(availCpus) && shouldShowCpuList ) {
        IndexRange      indexRange;

        IndexRangeInit(&indexRange);
        INFO(MessageOptionsNoMark|MessageOptionsNoNewline, "CPU ids adjacent to GPUs: ");
        BitVectorEnumerateQuick(availCpus, 0, BitIndexMax, printSetBitIndices, &indexRange);
        IndexRangePrint(&indexRange);
        INFO(MessageOptionsNoMark|MessageOptionsNoNewline|MessageOptionsNoLevelTag, "\n");
    }

    if ( gpuAssignments ) {
        GpuAssignment       *list = gpuAssignments;

        // We're good, make sure we exit that way:
        rc = 0;

        if ( MessageGetLevel() >= MessageVerbosityInfo ) {
            INFO(MessageOptionsNoMark|MessageOptionsNoNewline, "GPU-to-CPU mappings: ");
            while ( list ) {
                INFO(MessageOptionsNoMark|MessageOptionsNoNewline|MessageOptionsNoLevelTag, "%s%u", (list == gpuAssignments) ? "" : ",", list->gpuId);
                list = list->link;
            }
            INFO(MessageOptionsNoMark|MessageOptionsNoNewline|MessageOptionsNoLevelTag, "=");
            list = gpuAssignments;
            while ( list ) {
                INFO(MessageOptionsNoMark|MessageOptionsNoNewline|MessageOptionsNoLevelTag, "%s%u", (list == gpuAssignments) ? "" : ",", list->cpuId);
                list = list->link;
            }
            INFO(MessageOptionsNoMark|MessageOptionsNoNewline|MessageOptionsNoLevelTag, "\n");
        } else {
            while ( list ) {
                fprintf(stdout, "%s%u", (list == gpuAssignments) ? "" : ",", list->gpuId);
                list = list->link;
            }
            fputc('=', stdout);
            list = gpuAssignments;
            while ( list ) {
               fprintf(stdout, "%s%u", (list == gpuAssignments) ? "" : ",", list->cpuId);
                list = list->link;
            }
            fputc('\n', stdout);
        }
    }
    nvmlShutdown();
    return rc;
}

