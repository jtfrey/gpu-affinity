# gpu-affinity

Gaussian '16 support for GPU offload requires that jobs indicate to which CPU cores OpenMP threads should be bound and which cores should facilitate data movement and synchronization with the GPU(s) assigned to the run.  The can be inside the input file as link0 percent directives:

```
% CPU=0,1,2,3,17,18,19,20,21,35
% GPUCPU=0-1=17,35
```

or in the environment:

```
$ export GAUSS_CDEF=0,1,2,3,17,18,19,20,21,35
$ export GAUSS_GDEF=0-1=17,35
```

or even via command line flags:

```
$ g16 -c=0,1,2,3,17,18,19,20,21,35 -g=0-1=17,35 input.com ...
```

Whichever mechanism is employed to communicate these parameters to the Gaussian runtime, producing the list of CPU core ids and the mapping of each GPU device to one of those ids must be automated in shared environments.  Gaussian's own documentation on its GPU features posits that clusters are very uniform systems where the user always has full access to a node, so setting a boilerplate `%CPU` and `%GPUCPU` in your input files is sufficient.  Nothing could be further from the truth on many large, modern clusters.

## Slurm and CGroups

Our latest cluster at UD uses Slurm's CGroup support to restrict jobs to only those resources allocated to it by Slurm.  A cpuset is used to limit CPU cores; a memory group is used to limit physical memory usage; and a device group allows access only to the GPUs assigned to the job.  As it turns out, the nVidia userspace driver library (NVML) has an API to query which CPU cores have affinity with each GPU device.  With the CGroup restrictions mentioned above in place, the API only responds with CPUs and GPUs that are accessible.  Producing the CPU-to-GPU mapping is then a matter of selecting a core from each GPU's list of CPUs:

- if the system contains a GPU off each CPU socket and cores on each socket are in the cpuset, then one core per socket will be selected
- for the same system but with cores from a single socket in the cpuset, two cores from that socket will be selected

Obviously when using multiple GPUs it is more efficient to have CPU cores from all sockets that sit in front of the allocated GPUs.  Luckily, the GRES configuration in Slurm allows CPU ids to be associated with each GPU device and Slurm will attempt to schedule jobs such that each allocate GPU is accompanied by nearby core(s) — so long as a high enough CPU count was requested.

## The utility

The `gpu-affinity` utility has the following options:

```
usage:

  gpu-affinity {options}

 options:

  --help/-h                     show this information
  --verbose/-v                  display additional information on stderr as program
                                executes (can be used multiple times)
  --quiet/-q                    only print the desired output from this utility
  --show-cpu-list/-s            also show the list of all CPUs that are adjacent to
                                the available GPUs
  --max-cpu=#, -c=#             use this integer as the maximum number of CPUs; defaults
                                to 256, using the value 0 will request maximum possible
                                CPUs from the NUMA tools
  --mapping-pattern=<pattern>   when assigning CPUs to GPUs, choose CPU ids using the
       -p=<pattern>             given <pattern>; if there are more GPUs than letters in
                                the <pattern> assignment will cycle back to the start
                                of <pattern>

 Syntax of <pattern>

     A sequence of characters that specify how to select the primary CPU id that will be
     bound to each visible GPU device.  The following characters are understood:

         l, L, -                choose the lowest unused CPU id having affinity with
                                the device
         h, H, +                choose the highest unused CPU id having affinity with
                                the device

     The default <pattern> is 'hl'.
```

If successful, it writes a suitable `%GPUCPU` mapping to stdout:

```
$ gpu-affinity
0,1=53,71
```

Bash code to capture this output and make use of it:

```
GAUSS_GDEF="$(gpu-affinity)"
if [ $? -ne 0 ]; then
    echo "ERROR:  unable to determine GPU-to-CPU mapping: $GAUSS_GDEF"
    exit 1
fi
export GAUSS_GDEF
```
