//
// BitVector.h
//
// A simple array of bits.
//

#ifndef __BITVECTOR_H__
#define __BITVECTOR_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <limits.h>
#include <string.h>
#include <errno.h>

typedef unsigned long BitVectorSubType;

#define BitVectorBitsPerSubType (8 * sizeof(BitVectorSubType))

#define BitIndexMax UINT_MAX

typedef struct BitVector * BitVectorRef;

typedef struct BitVectorEnumState {
    unsigned int        startBitIndex, endBitIndex;

    unsigned int        curBitIndex;
    unsigned int        startArrayMajorIndex, startArrayMinorIndex;
    unsigned int        endArrayMajorIndex, endArrayMinorIndex;
    unsigned int        curArrayMajorIndex, curArrayMinorIndex;
} BitVectorEnumState;

typedef bool (*BitVectorEnumerator)(BitVectorRef bitVector, unsigned int index, bool value, const void *context);

BitVectorRef BitVectorCreate(unsigned int capacity);
BitVectorRef BitVectorCopy(BitVectorRef bitVector);
void BitVectorDestroy(BitVectorRef bitVector);

BitVectorSubType* BitVectorGetBitArrayPtr(BitVectorRef bitVector);
unsigned int BitVectorGetBitCapacity(BitVectorRef bitVector);
unsigned int BitVectorGetArrayLength(BitVectorRef bitVector);

bool BitVectorIsZero(BitVectorRef bitVector);
bool BitVectorIsEqual(BitVectorRef bitVector1, BitVectorRef bitVector2);

bool BitVectorGetBit(BitVectorRef bitVector, unsigned int bitIndex, bool *bitValue);
bool BitVectorGetBitOnly(BitVectorRef bitVector, unsigned int bitIndex);

bool BitVectorEnumerate(BitVectorRef bitVector, BitVectorEnumState *enumState, bool shouldContinue, BitVectorEnumerator enumeratorFunction, const void *context);
bool BitVectorReverseEnumerate(BitVectorRef bitVector, BitVectorEnumState *enumState, bool shouldContinue, BitVectorEnumerator enumeratorFunction, const void *context);

bool BitVectorEnumerateQuick(BitVectorRef bitVector, unsigned int startBitIndex, unsigned int endBitIndex, BitVectorEnumerator enumeratorFunction, const void *context);
bool BitVectorReverseEnumerateQuick(BitVectorRef bitVector, unsigned int startBitIndex, unsigned int endBitIndex, BitVectorEnumerator enumeratorFunction, const void *context);

bool BitVectorHighestBitWithValue(BitVectorRef bitVector, bool bitValue, unsigned int *outBitIndex);
bool BitVectorHighestBitWithValueInRange(BitVectorRef bitVector, bool bitValue, unsigned int startBitIndex, unsigned int endBitIndex, unsigned int *outBitIndex);
bool BitVectorLowestBitWithValue(BitVectorRef bitVector, bool bitValue, unsigned int *outBitIndex);
bool BitVectorLowestBitWithValueInRange(BitVectorRef bitVector, bool bitValue, unsigned int startBitIndex, unsigned int endBitIndex, unsigned int *outBitIndex);

void BitVectorClear(BitVectorRef bitVector);
bool BitVectorSetBit(BitVectorRef bitVector, unsigned int bitIndex, bool bitValue);
bool BitVectorSetBits(BitVectorRef bitVector, unsigned int startBitIndex, unsigned int endBitIndex, bool bitValue);
BitVectorRef BitVectorAnd(BitVectorRef bitVector1, BitVectorRef bitVector2, BitVectorRef outBitVector);
BitVectorRef BitVectorOr(BitVectorRef bitVector1, BitVectorRef bitVector2, BitVectorRef outBitVector);
BitVectorRef BitVectorXor(BitVectorRef bitVector1, BitVectorRef bitVector2, BitVectorRef outBitVector);
BitVectorRef BitVectorNot(BitVectorRef bitVector1, BitVectorRef outBitVector);

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* __BITVECTOR_H__ */
